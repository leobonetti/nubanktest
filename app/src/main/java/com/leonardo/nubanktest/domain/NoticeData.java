package com.leonardo.nubanktest.domain;

import com.google.gson.annotations.SerializedName;
import com.leonardo.nubanktest.constants.Constants;
import com.leonardo.nubanktest.domain.Action;
import com.leonardo.nubanktest.domain.Link;

/**
 * Created by Sacra on 3/13/16.
 */
public class NoticeData extends Link {

    @SerializedName("title")
    private String title;

    @SerializedName("description")
    private String description;

    @SerializedName("primary_action")
    private Action primaryAction;

    @SerializedName("secondary_action")
    private Action secondaryAction;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Action getPrimaryAction() {
        return primaryAction;
    }

    public void setPrimaryAction(Action primaryAction) {
        this.primaryAction = primaryAction;
    }

    public Action getSecondaryAction() {
        return secondaryAction;
    }

    public void setSecondaryAction(Action secondaryAction) {
        this.secondaryAction = secondaryAction;
    }

    private boolean isLinksInvalid() {
        return this.links.get(Constants.END_POINT_CHARGEBACK_REFERENCE) == null;
    }

    public boolean isDomainInvalid() {
        return this.title == null || this.description == null || primaryAction == null || secondaryAction == null || this.links == null || this.isLinksInvalid();
    }

}
