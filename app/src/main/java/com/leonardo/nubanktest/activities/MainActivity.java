package com.leonardo.nubanktest.activities;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.leonardo.nubanktest.R;
import com.leonardo.nubanktest.fragments.ChargebackSuccessFragment;
import com.leonardo.nubanktest.fragments.HomeFragment;
import com.leonardo.nubanktest.helpers.FragmentHelper;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentHelper.replaceFragment(MainActivity.this, HomeFragment.newInstance(), HomeFragment.class.getSimpleName(), R.id.main_activity_fragment);
    }

    @Override
    public void onBackPressed() {
        Fragment f = getFragmentManager().findFragmentById(R.id.main_activity_fragment);
        if (f instanceof ChargebackSuccessFragment) {
            FragmentHelper.popBackstatckToFragment(MainActivity.this, HomeFragment.class.getSimpleName());
            return;
        }

        if (getFragmentManager().getBackStackEntryCount() == 1) {
            this.finish();
        } else {
            getFragmentManager().popBackStack();
        }
    }
}
