package com.leonardo.nubanktest.helpers;


import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;

/**
 * Created by Sacra on 1/24/16.
 */
public class FragmentHelper {

    public static void addFragment(Activity activity, Fragment newFragment, String backStackName, int fragmentID) {
        FragmentTransaction fragmentTransaction = activity.getFragmentManager().beginTransaction();
        fragmentTransaction.add(fragmentID, newFragment);
        fragmentTransaction.addToBackStack(backStackName);
        fragmentTransaction.commit();
    }

    public static void replaceFragment(Activity activity, Fragment newFragment, String backStackName, int fragmentID) {
        FragmentTransaction fragmentTransaction = activity.getFragmentManager().beginTransaction();
        fragmentTransaction.replace(fragmentID, newFragment);
        fragmentTransaction.addToBackStack(backStackName);
        fragmentTransaction.commit();
    }

    public static void popFragment(Activity activity) {
        FragmentManager fragmentManager = activity.getFragmentManager();
        fragmentManager.popBackStack();
    }

    public static void popBackstatckToFragment(Activity activity, String fragmentName) {
        FragmentManager fragmentManager = activity.getFragmentManager();
        fragmentManager.popBackStack(fragmentName, 0);
    }
}
