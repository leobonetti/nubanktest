package com.leonardo.nubanktest.domain;

import com.google.gson.annotations.SerializedName;
import com.leonardo.nubanktest.constants.Constants;

import java.util.List;

/**
 * Created by Sacra on 3/13/16.
 */
public class ChargebackData extends Link {

    @SerializedName("comment_hint")
    private String commentHint;

    @SerializedName("id")
    private String id;

    @SerializedName("title")
    private String title;

    @SerializedName("autoblock")
    private Boolean autoblock;

    @SerializedName("reason_details")
    private List<ReasonDetail> listReasonDetails;

    public String getCommentHint() {
        return commentHint;
    }

    public void setCommentHint(String commentHint) {
        this.commentHint = commentHint;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getAutoblock() {
        return autoblock;
    }

    public void setAutoblock(Boolean autoblock) {
        this.autoblock = autoblock;
    }

    public List<ReasonDetail> getListReasonDetails() {
        return listReasonDetails;
    }

    public void setListReasonDetails(List<ReasonDetail> listReasonDetails) {
        this.listReasonDetails = listReasonDetails;
    }

    private boolean isReasonDetailsInvalid() {
        for(ReasonDetail reasonDetail : this.listReasonDetails) {
            if(reasonDetail.isDomainInvalid()) {
                return true;
            }
        }

        return false;
    }

    private boolean isLinksInvalid() {
        return this.links.get(Constants.END_POINT_BLOCK_CARD_REFERENCE) == null ||
                this.links.get(Constants.END_POINT_UNBLOCK_CARD_REFERENCE) == null ||
                this.links.get(Constants.END_POINT_CHARGEBACK_SELF_REFERENCE) == null;
    }

    public boolean isDomainInvalid() {
        return this.commentHint == null ||
                this.id == null ||
                this.title == null ||
                this.autoblock == null ||
                this.listReasonDetails == null ||
                this.isReasonDetailsInvalid() ||
                this.links == null ||
                this.isLinksInvalid();
    }
}
