package com.leonardo.nubanktest.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.leonardo.nubanktest.R;
import com.leonardo.nubanktest.domain.NoticeData;
import com.leonardo.nubanktest.helpers.DialogHelper;
import com.leonardo.nubanktest.helpers.GsonHelper;
import com.leonardo.nubanktest.viewmodel.NoticeViewModel;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Sacra on 3/9/16.
 */
public class NoticeFragment extends Fragment {

    private static final String NOTICE_DATA_ARGS = "NoticeDataJson";

    @Bind(R.id.fragment_notice_txt_title)
    TextView txtTitle;

    @Bind(R.id.fragment_notice_txt_description)
    TextView txtDescription;

    @Bind(R.id.fragment_notice_btn_primary_action)
    TextView btnPrimaryAction;

    @Bind(R.id.fragment_notice_btn_secondary_action)
    TextView btnSecondaryAction;

    private NoticeViewModel viewModel;
    private NoticeData noticeData;

    public static NoticeFragment newInstance(String noticeDataJson) {
        NoticeFragment fragment = new NoticeFragment();

        Bundle args = new Bundle();
        args.putString(NOTICE_DATA_ARGS, noticeDataJson);
        fragment.setArguments(args);

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notice, container, false);

        ButterKnife.bind(this, view);

        this.viewModel = new NoticeViewModel();

        this.noticeData = GsonHelper.fromJson(getArguments().getString(NOTICE_DATA_ARGS), NoticeData.class);
        this.initUI();

        return view;
    }

    private void initUI() {
        this.txtTitle.setText(this.noticeData.getTitle());
        this.txtDescription.setText(Html.fromHtml(this.noticeData.getDescription()));
        this.btnPrimaryAction.setText(this.noticeData.getPrimaryAction().getTitle());
        this.btnSecondaryAction.setText(this.noticeData.getSecondaryAction().getTitle());
    }

    @OnClick(R.id.fragment_notice_btn_primary_action)
    public void onContinueTapped() {
        this.viewModel.handleAction(this.noticeData.getPrimaryAction(), this, this.noticeData);
    }

    @OnClick(R.id.fragment_notice_btn_secondary_action)
    public void onCancelTapped() {
        this.viewModel.handleAction(noticeData.getSecondaryAction(), this, this.noticeData);
    }

    public void onError(String errorMessage) {
        DialogHelper.showNeutralDialog(this.getActivity(), errorMessage);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        ButterKnife.unbind(this);
    }
}
