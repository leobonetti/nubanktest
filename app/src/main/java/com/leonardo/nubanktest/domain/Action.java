package com.leonardo.nubanktest.domain;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Sacra on 3/13/16.
 */
public class Action {

    @SerializedName("title")
    private String title;

    @SerializedName("action")
    private String action;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
