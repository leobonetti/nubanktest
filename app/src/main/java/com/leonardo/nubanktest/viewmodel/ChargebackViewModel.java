package com.leonardo.nubanktest.viewmodel;

import com.leonardo.nubanktest.R;
import com.leonardo.nubanktest.constants.Constants;
import com.leonardo.nubanktest.domain.ChargebackData;
import com.leonardo.nubanktest.domain.EndPoint;
import com.leonardo.nubanktest.domain.Status;
import com.leonardo.nubanktest.fragments.ChargebackFragment;
import com.leonardo.nubanktest.fragments.ChargebackSuccessFragment;
import com.leonardo.nubanktest.helpers.ConnectivityHelper;
import com.leonardo.nubanktest.helpers.DialogHelper;
import com.leonardo.nubanktest.helpers.FragmentHelper;
import com.leonardo.nubanktest.webservice.ChargebackService;
import com.leonardo.nubanktest.webservice.Webservice;
import com.leonardo.nubanktest.webservice.request.SaveContestRequest;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Sacra on 3/13/16.
 */
public class ChargebackViewModel {

    public void blockCardIfNecessary(ChargebackFragment fragment, ChargebackData chargebackData) {
        if (chargebackData.getAutoblock()) {
            this.blockCard(fragment, chargebackData);
        }
    }

    public void blockCard(ChargebackFragment fragment, ChargebackData chargebackData) {
        if (ConnectivityHelper.isOnline(fragment.getActivity())) {
            this.callBlockCardService(fragment, chargebackData.getLinks().get(Constants.END_POINT_BLOCK_CARD_REFERENCE));
        } else {
            fragment.onError(fragment.getString(R.string.error_default_no_connection));
        }
    }

    private void callBlockCardService(final ChargebackFragment fragment, EndPoint endPoint) {
        DialogHelper.showProgressBarDialog(fragment.getActivity());

        ChargebackService service = Webservice.getInstance().getRetrofitInstance().create(ChargebackService.class);
        Call<Status> callBlockCard = service.blockCard(endPoint.getHref());
        callBlockCard.enqueue(new Callback<Status>() {
            @Override
            public void onResponse(Call<Status> call, Response<Status> response) {

                if (response.isSuccess()) {
                    analyzeBlockCardServiceResponse(fragment, response.body());
                } else {
                    try {
                        fragment.onError(response.errorBody().string());
                    } catch (IOException e) {
                        fragment.onError(fragment.getString(R.string.error_default_no_connection));
                    }
                }
                DialogHelper.dismissProgressBarDialog();
            }

            @Override
            public void onFailure(Call<Status> call, Throwable t) {
                DialogHelper.dismissProgressBarDialog();

                fragment.onError(fragment.getString(R.string.error_default_no_connection));
            }
        });
    }

    private void analyzeBlockCardServiceResponse(ChargebackFragment fragment, Status status) {
        if (status == null || status.isDomainInvalid() || !status.getStatus().equals(Constants.STATUS_OK)) {
            fragment.onError(fragment.getString(R.string.error_incorrect_answer));

            return;
        }

        fragment.onBlockCardSuccess();
    }

    public void unblockCard(ChargebackFragment fragment, ChargebackData chargebackData) {
        if (ConnectivityHelper.isOnline(fragment.getActivity())) {
            this.callUnblockCardService(fragment, chargebackData.getLinks().get(Constants.END_POINT_UNBLOCK_CARD_REFERENCE));
        } else {
            fragment.onError(fragment.getString(R.string.error_default_no_connection));
        }
    }

    private void callUnblockCardService(final ChargebackFragment fragment, EndPoint endPoint) {
        DialogHelper.showProgressBarDialog(fragment.getActivity());

        ChargebackService service = Webservice.getInstance().getRetrofitInstance().create(ChargebackService.class);
        Call<Status> callUnblockCard = service.unblockCard(endPoint.getHref());
        callUnblockCard.enqueue(new Callback<Status>() {
            @Override
            public void onResponse(Call<Status> call, Response<Status> response) {

                if (response.isSuccess()) {
                    analyzeUnblockCardServiceResponse(fragment, response.body());
                } else {
                    try {
                        fragment.onError(response.errorBody().string());
                    } catch (IOException e) {
                        fragment.onError(fragment.getString(R.string.error_default_no_connection));
                    }
                }
                DialogHelper.dismissProgressBarDialog();
            }

            @Override
            public void onFailure(Call<Status> call, Throwable t) {
                DialogHelper.dismissProgressBarDialog();

                fragment.onError(fragment.getString(R.string.error_default_no_connection));
            }
        });
    }

    private void analyzeUnblockCardServiceResponse(ChargebackFragment fragment, Status status) {
        if (status == null || status.isDomainInvalid() || !status.getStatus().equals(Constants.STATUS_OK)) {
            fragment.onError(fragment.getString(R.string.error_incorrect_answer));

            return;
        }

        fragment.onUnblockCardSuccess();
    }

    public void saveContest(ChargebackFragment fragment, ChargebackData chargebackData, String comment) {
        if (ConnectivityHelper.isOnline(fragment.getActivity())) {
            SaveContestRequest saveContestRequest = new SaveContestRequest();
            saveContestRequest.setComment(comment);
            saveContestRequest.setListReasonDetails(chargebackData.getListReasonDetails());

            this.callSaveContestService(fragment, chargebackData.getLinks().get(Constants.END_POINT_CHARGEBACK_SELF_REFERENCE), saveContestRequest);
        } else {
            fragment.onError(fragment.getString(R.string.error_default_no_connection));
        }
    }

    private void callSaveContestService(final ChargebackFragment fragment, EndPoint endPoint, SaveContestRequest saveContestRequest) {
        DialogHelper.showProgressBarDialog(fragment.getActivity());

        ChargebackService service = Webservice.getInstance().getRetrofitInstance().create(ChargebackService.class);
        Call<Status> callSaveContest = service.saveConstest(endPoint.getHref(), saveContestRequest);
        callSaveContest.enqueue(new Callback<Status>() {
            @Override
            public void onResponse(Call<Status> call, Response<Status> response) {

                if (response.isSuccess()) {
                    analyzeSaveContestServiceResponse(fragment, response.body());
                } else {
                    try {
                        fragment.onError(response.errorBody().string());
                    } catch (IOException e) {
                        fragment.onError(fragment.getString(R.string.error_default_no_connection));
                    }
                }
                DialogHelper.dismissProgressBarDialog();
            }

            @Override
            public void onFailure(Call<Status> call, Throwable t) {
                DialogHelper.dismissProgressBarDialog();

                fragment.onError(fragment.getString(R.string.error_default_no_connection));
            }
        });
    }

    private void analyzeSaveContestServiceResponse(ChargebackFragment fragment, Status status) {
        if (status == null || status.isDomainInvalid() || !status.getStatus().equals(Constants.STATUS_OK)) {
            fragment.onError(fragment.getString(R.string.error_incorrect_answer));

            return;
        }

        this.goToChargebackSuccessFragment(fragment);
    }

    private void goToChargebackSuccessFragment(ChargebackFragment fragment) {
        FragmentHelper.replaceFragment(fragment.getActivity(), ChargebackSuccessFragment.newInstance(), ChargebackSuccessFragment.class.getSimpleName(), R.id.main_activity_fragment);
    }

    public void onCancel(ChargebackFragment fragment) {
        FragmentHelper.popFragment(fragment.getActivity());
    }
}
