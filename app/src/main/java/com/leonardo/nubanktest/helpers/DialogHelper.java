package com.leonardo.nubanktest.helpers;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.Window;

import com.leonardo.nubanktest.R;

/**
 * Created by Sacra on 2/9/16.
 */
public class DialogHelper {

    private static AlertDialog loadingDialog;

    public static void showNeutralDialog(Context context, String message) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    public static void showProgressBarDialog(Context context) {
        loadingDialog = new AlertDialog.Builder(context).create();
        loadingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        loadingDialog.setCancelable(false);
        loadingDialog.show();
        loadingDialog.setContentView(R.layout.dialog_loading);
    }

    public static void dismissProgressBarDialog() {
        if (loadingDialog != null) {
            loadingDialog.dismiss();
        }
    }

}
