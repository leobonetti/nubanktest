package com.leonardo.nubanktest.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kyleduo.switchbutton.SwitchButton;
import com.leonardo.nubanktest.R;
import com.leonardo.nubanktest.domain.ChargebackData;
import com.leonardo.nubanktest.domain.ReasonDetail;
import com.leonardo.nubanktest.helpers.DialogHelper;
import com.leonardo.nubanktest.helpers.GsonHelper;
import com.leonardo.nubanktest.helpers.KeyboardHelper;
import com.leonardo.nubanktest.viewmodel.ChargebackViewModel;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Sacra on 3/10/16.
 */
public class ChargebackFragment extends Fragment {

    private static final String CHARGEBACK_DATA_ARGS = "ChargebackDataJson";

    @Bind(R.id.fragment_chargeback_txt_title)
    TextView txtTitle;

    @Bind(R.id.fragment_chargeback_ll_reason_details_container)
    LinearLayout llReasonDetailsContainer;

    @Bind(R.id.fragment_chargeback_ll_locked)
    RelativeLayout llLockedCard;

    @Bind(R.id.fragment_chargeback_ll_unlocked)
    RelativeLayout llUnlockedCard;

    @Bind(R.id.fragment_chargeback_edt_details)
    EditText edtDetails;

    @Bind(R.id.fragment_chargeback_btn_contest_disabled)
    TextView btnContestDisabled;

    @Bind(R.id.fragment_chargeback_btn_contest_enabled)
    TextView btnContestEnabled;

    private final TextWatcher twDetails = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            int btnEDisabledVisible = View.VISIBLE;
            int btnEnabledVisible = View.GONE;

            if (s.length() != 0) {
                btnEDisabledVisible = View.GONE;
                btnEnabledVisible = View.VISIBLE;
            }

            btnContestDisabled.setVisibility(btnEDisabledVisible);
            btnContestEnabled.setVisibility(btnEnabledVisible);
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    private final CompoundButton.OnCheckedChangeListener cbListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            View view = (View) buttonView.getTag();
            ReasonDetail reasonDetail = chargebackData.getListReasonDetails().get((Integer) view.getTag());
            reasonDetail.setResponse(isChecked);

            int color = R.color.textColor;
            if(isChecked) {
                color = R.color.greenColor;
            }

            ((TextView) view.findViewById(R.id.fragment_chargeback_reason_details_txt_title)).setTextColor(ContextCompat.getColor(ChargebackFragment.this.getActivity(), color));
        }
    };

    private ChargebackData chargebackData;
    private ChargebackViewModel viewModel;

    public static ChargebackFragment newInstance(String chargebackJson) {
        ChargebackFragment fragment = new ChargebackFragment();

        Bundle args = new Bundle();
        args.putString(CHARGEBACK_DATA_ARGS, chargebackJson);
        fragment.setArguments(args);

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chargeback, container, false);

        ButterKnife.bind(this, view);

        this.chargebackData = GsonHelper.fromJson(getArguments().getString(CHARGEBACK_DATA_ARGS), ChargebackData.class);
        this.initUI();

        this.viewModel = new ChargebackViewModel();
        this.viewModel.blockCardIfNecessary(this, this.chargebackData);

        return view;
    }

    private void initUI() {
        this.txtTitle.setText(this.chargebackData.getTitle());

        this.addReasonDetails(this.chargebackData);

        this.edtDetails.setHint(Html.fromHtml(this.chargebackData.getCommentHint()));
        this.edtDetails.addTextChangedListener(this.twDetails);

    }

    private void addReasonDetails(ChargebackData chargebackData) {
        List<ReasonDetail> listReasonDetail = chargebackData.getListReasonDetails();
        int count = listReasonDetail.size();

        for (int i = 0; i < count; i++) {
            ReasonDetail reasonDetail = listReasonDetail.get(i);

            View view = View.inflate(this.llReasonDetailsContainer.getContext(), R.layout.fragment_chargeback_reason_details, null);
            view.setTag(i);

            ((TextView) view.findViewById(R.id.fragment_chargeback_reason_details_txt_title)).setText(reasonDetail.getTitle());
            SwitchButton switchReason = (SwitchButton) view.findViewById(R.id.fragment_chargeback_reason_details_switch);
            switchReason.setTag(view);
            switchReason.setOnCheckedChangeListener(this.cbListener);

            this.llReasonDetailsContainer.addView(view);
        }
    }

    @OnClick(R.id.fragment_chargeback_ll_locked)
    public void onLockedCardTapped() {
        this.viewModel.unblockCard(this, this.chargebackData);
    }

    @OnClick(R.id.fragment_chargeback_ll_unlocked)
    public void onUnockedCardTapped() {
        this.viewModel.blockCard(this, this.chargebackData);
    }

    @OnClick(R.id.fragment_chargeback_btn_contest_enabled)
    public void onContestTapped() {
        KeyboardHelper.hideKeyboard(this.getActivity());
        this.viewModel.saveContest(this, this.chargebackData, this.edtDetails.getText().toString());
    }

    @OnClick(R.id.fragment_chargeback_btn_cancel)
    public void onCancelTapped() {
        KeyboardHelper.hideKeyboard(this.getActivity());
        this.viewModel.onCancel(this);
    }

    public void onBlockCardSuccess() {
        this.llLockedCard.setVisibility(View.VISIBLE);
        this.llUnlockedCard.setVisibility(View.GONE);
    }

    public void onUnblockCardSuccess() {
        this.llLockedCard.setVisibility(View.GONE);
        this.llUnlockedCard.setVisibility(View.VISIBLE);
    }

    public void onError(String errorMessage) {
        DialogHelper.showNeutralDialog(this.getActivity(), errorMessage);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        ButterKnife.unbind(this);
    }
}
