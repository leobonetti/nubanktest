package com.leonardo.nubanktest.constants;

/**
 * Created by Sacra on 3/12/16.
 */
public class Constants {

//    Static URLs
    public static final String STATIC_URL = "https://nu-mobile-hiring.herokuapp.com";

//    Endpoints
    public static final String END_POINT_NOTICE_REFERENCE = "notice";
    public static final String END_POINT_CHARGEBACK_REFERENCE = "chargeback";
    public static final String END_POINT_BLOCK_CARD_REFERENCE = "block_card";
    public static final String END_POINT_UNBLOCK_CARD_REFERENCE = "unblock_card";
    public static final String END_POINT_CHARGEBACK_SELF_REFERENCE = "self";

//    Action
    public static final String ACTION_CONTINUE = "continue";
    public static final String ACTION_CANCEL = "cancel";

//    Status
    public static final String STATUS_OK = "Ok";
}
