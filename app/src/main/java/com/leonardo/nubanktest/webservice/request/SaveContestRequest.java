package com.leonardo.nubanktest.webservice.request;

import com.google.gson.annotations.SerializedName;
import com.leonardo.nubanktest.domain.ReasonDetail;

import java.util.List;

/**
 * Created by Sacra on 3/13/16.
 */
public class SaveContestRequest {

    @SerializedName("comment")
    private String comment;

    @SerializedName("reason_details")
    private List<ReasonDetail> listReasonDetails;

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public List<ReasonDetail> getListReasonDetails() {
        return listReasonDetails;
    }

    public void setListReasonDetails(List<ReasonDetail> listReasonDetails) {
        this.listReasonDetails = listReasonDetails;
    }
}
