package com.leonardo.nubanktest.domain;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;

/**
 * Created by Sacra on 3/12/16.
 */
public class Link {

    @SerializedName("links")
    protected HashMap<String, EndPoint> links;

    public HashMap<String, EndPoint> getLinks() {
        return links;
    }

    public void setLinks(HashMap<String, EndPoint> links) {
        this.links = links;
    }

    public boolean isDomainInvalid() {
        return this.links == null;
    }
}
