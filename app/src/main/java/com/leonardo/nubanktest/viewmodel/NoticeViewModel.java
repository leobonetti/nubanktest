package com.leonardo.nubanktest.viewmodel;

import com.leonardo.nubanktest.R;
import com.leonardo.nubanktest.constants.Constants;
import com.leonardo.nubanktest.domain.Action;
import com.leonardo.nubanktest.domain.ChargebackData;
import com.leonardo.nubanktest.domain.EndPoint;
import com.leonardo.nubanktest.domain.NoticeData;
import com.leonardo.nubanktest.fragments.ChargebackFragment;
import com.leonardo.nubanktest.fragments.NoticeFragment;
import com.leonardo.nubanktest.helpers.ConnectivityHelper;
import com.leonardo.nubanktest.helpers.DialogHelper;
import com.leonardo.nubanktest.helpers.FragmentHelper;
import com.leonardo.nubanktest.helpers.GsonHelper;
import com.leonardo.nubanktest.webservice.ChargebackService;
import com.leonardo.nubanktest.webservice.Webservice;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Sacra on 3/13/16.
 */
public class NoticeViewModel {

    public void handleAction(Action action, NoticeFragment fragment, NoticeData noticeData) {
        if (action.getAction().equals(Constants.ACTION_CONTINUE)) {
            this.getChargebackData(fragment, noticeData.getLinks().get(Constants.END_POINT_CHARGEBACK_REFERENCE));
            return;
        }

        if (action.getAction().equals(Constants.ACTION_CANCEL)) {
            FragmentHelper.popFragment(fragment.getActivity());

            return;
        }

        FragmentHelper.popFragment(fragment.getActivity());
    }

    private void getChargebackData(NoticeFragment fragment, EndPoint endPoint) {
        if (ConnectivityHelper.isOnline(fragment.getActivity())) {
            this.callChargebackDataService(fragment, endPoint);
        } else {
            fragment.onError(fragment.getString(R.string.error_default_no_connection));
        }
    }

    private void callChargebackDataService(final NoticeFragment fragment, EndPoint endPoint) {
        DialogHelper.showProgressBarDialog(fragment.getActivity());

        ChargebackService service = Webservice.getInstance().getRetrofitInstance().create(ChargebackService.class);
        Call<ChargebackData> callGetChargeBackData = service.getChargebackData(endPoint.getHref());
        callGetChargeBackData.enqueue(new Callback<ChargebackData>() {
            @Override
            public void onResponse(Call<ChargebackData> call, Response<ChargebackData> response) {

                if (response.isSuccess()) {
                    analyzeChargebackDataServiceResponse(fragment, response.body());
                } else {
                    try {
                        fragment.onError(response.errorBody().string());
                    } catch (IOException e) {
                        fragment.onError(fragment.getString(R.string.error_default_no_connection));
                    }
                }
                DialogHelper.dismissProgressBarDialog();
            }

            @Override
            public void onFailure(Call<ChargebackData> call, Throwable t) {
                DialogHelper.dismissProgressBarDialog();

                fragment.onError(fragment.getString(R.string.error_default_no_connection));
            }
        });
    }

    private void analyzeChargebackDataServiceResponse(NoticeFragment fragment, ChargebackData chargebackData) {
        if (chargebackData == null || chargebackData.isDomainInvalid()) {
            fragment.onError(fragment.getString(R.string.error_incorrect_answer));

            return;
        }

        this.goToChargeBackFragment(fragment, chargebackData);
    }

    private void goToChargeBackFragment(NoticeFragment fragment, ChargebackData chargebackData) {
        String chargebackDataJson = GsonHelper.toJson(chargebackData, ChargebackData.class);
        FragmentHelper.replaceFragment(fragment.getActivity(), ChargebackFragment.newInstance(chargebackDataJson), ChargebackFragment.class.getSimpleName(), R.id.main_activity_fragment);
    }
}
