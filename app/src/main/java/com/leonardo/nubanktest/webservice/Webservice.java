package com.leonardo.nubanktest.webservice;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.leonardo.nubanktest.constants.Constants;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Sacra on 3/12/16.
 */
public class Webservice {

    private static Webservice instance = null;
    private final Retrofit retrofitInstance;

    private Webservice() {
        Gson gson = new GsonBuilder()
                .setDateFormat("dd/MM/yyyy HH:mm:ss")
                .create();

        this.retrofitInstance = new Retrofit.Builder()
                .baseUrl(Constants.STATIC_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }

    public static Webservice getInstance() {
        if (instance == null) {
            instance = new Webservice();
        }

        return instance;
    }

    public Retrofit getRetrofitInstance() {
        return this.retrofitInstance;
    }
}

