package com.leonardo.nubanktest.webservice;

import com.leonardo.nubanktest.domain.NoticeData;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * Created by Sacra on 3/12/16.
 */
public interface NoticeService {

    @GET
    Call<NoticeData> getNoticeData(@Url String url);
}
