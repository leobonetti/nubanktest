package com.leonardo.nubanktest.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.leonardo.nubanktest.R;
import com.leonardo.nubanktest.helpers.FragmentHelper;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Sacra on 3/12/16.
 */
public class ChargebackSuccessFragment extends Fragment {

    public static ChargebackSuccessFragment newInstance() {
        return new ChargebackSuccessFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chargeback_success, container, false);

        ButterKnife.bind(this, view);

        return view;
    }

    @OnClick(R.id.fragment_chargeback_success_btn_close)
    public void onCloseTapped() {
        FragmentHelper.popBackstatckToFragment(this.getActivity(), HomeFragment.class.getSimpleName());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        ButterKnife.unbind(this);
    }
}
