package com.leonardo.nubanktest.viewmodel;

import com.leonardo.nubanktest.constants.Constants;
import com.leonardo.nubanktest.R;
import com.leonardo.nubanktest.domain.EndPoint;
import com.leonardo.nubanktest.domain.Link;
import com.leonardo.nubanktest.domain.NoticeData;
import com.leonardo.nubanktest.fragments.HomeFragment;
import com.leonardo.nubanktest.fragments.NoticeFragment;
import com.leonardo.nubanktest.helpers.ConnectivityHelper;
import com.leonardo.nubanktest.helpers.DialogHelper;
import com.leonardo.nubanktest.helpers.FragmentHelper;
import com.leonardo.nubanktest.helpers.GsonHelper;
import com.leonardo.nubanktest.webservice.NoticeService;
import com.leonardo.nubanktest.webservice.StaticService;
import com.leonardo.nubanktest.webservice.Webservice;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Sacra on 3/12/16.
 */
public class HomeViewModel {

    public void getNoticeEndPoint(HomeFragment fragment) {
        if (ConnectivityHelper.isOnline(fragment.getActivity())) {
            this.callNoticeEndPointService(fragment);
        } else {
            fragment.onError(fragment.getString(R.string.error_default_no_connection));
        }
    }

    private void callNoticeEndPointService(final HomeFragment fragment) {
        DialogHelper.showProgressBarDialog(fragment.getActivity());

        StaticService service = Webservice.getInstance().getRetrofitInstance().create(StaticService.class);
        Call<Link> callGetNoticeEndPoint = service.getNoticeEndPoint(Constants.STATIC_URL);
        callGetNoticeEndPoint.enqueue(new Callback<Link>() {
            @Override
            public void onResponse(Call<Link> call, Response<Link> response) {

                if (response.isSuccess()) {
                    analyzeNoticeEndPointServiceResponse(fragment, response.body());
                } else {
                    DialogHelper.dismissProgressBarDialog();

                    try {
                        fragment.onError(response.errorBody().string());
                    } catch (IOException e) {
                        fragment.onError(fragment.getString(R.string.error_default_no_connection));
                    }
                }
            }

            @Override
            public void onFailure(Call<Link> call, Throwable t) {
                DialogHelper.dismissProgressBarDialog();

                fragment.onError(fragment.getString(R.string.error_default_no_connection));
            }
        });
    }

    private void analyzeNoticeEndPointServiceResponse(HomeFragment fragment, Link link) {
        if (link == null || link.isDomainInvalid() || link.getLinks().get(Constants.END_POINT_NOTICE_REFERENCE) == null) {
            DialogHelper.dismissProgressBarDialog();
            fragment.onError(fragment.getString(R.string.error_incorrect_answer));

            return;
        }

        this.getNoticeData(fragment, link.getLinks().get(Constants.END_POINT_NOTICE_REFERENCE));
    }

    private void getNoticeData(HomeFragment fragment, EndPoint endPoint) {
        if (ConnectivityHelper.isOnline(fragment.getActivity())) {
            this.callNoticeDataService(fragment, endPoint);
        } else {
            fragment.onError(fragment.getString(R.string.error_default_no_connection));
        }
    }

    private void callNoticeDataService(final HomeFragment fragment, EndPoint endPoint) {
        NoticeService service = Webservice.getInstance().getRetrofitInstance().create(NoticeService.class);
        Call<NoticeData> callGetNoticeData = service.getNoticeData(endPoint.getHref());
        callGetNoticeData.enqueue(new Callback<NoticeData>() {
            @Override
            public void onResponse(Call<NoticeData> call, Response<NoticeData> response) {

                if (response.isSuccess()) {
                    analyzeNoticeDataServiceResponse(fragment, response.body());
                } else {
                    try {
                        fragment.onError(response.errorBody().string());
                    } catch (IOException e) {
                        fragment.onError(fragment.getString(R.string.error_default_no_connection));
                    }
                }
                DialogHelper.dismissProgressBarDialog();
            }

            @Override
            public void onFailure(Call<NoticeData> call, Throwable t) {
                DialogHelper.dismissProgressBarDialog();

                fragment.onError(fragment.getString(R.string.error_default_no_connection));
            }
        });
    }

    private void analyzeNoticeDataServiceResponse(HomeFragment fragment, NoticeData noticeData) {
        if (noticeData == null || noticeData.isDomainInvalid()) {
            fragment.onError(fragment.getString(R.string.error_incorrect_answer));

            return;
        }

        this.goToNoticeFragment(fragment, noticeData);
    }

    private void goToNoticeFragment(HomeFragment fragment, NoticeData noticeData) {
        String noticeDataJson = GsonHelper.toJson(noticeData, NoticeData.class);
        FragmentHelper.replaceFragment(fragment.getActivity(), NoticeFragment.newInstance(noticeDataJson), NoticeFragment.class.getSimpleName(), R.id.main_activity_fragment);
    }
}
