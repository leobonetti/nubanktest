package com.leonardo.nubanktest.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.leonardo.nubanktest.R;
import com.leonardo.nubanktest.helpers.DialogHelper;
import com.leonardo.nubanktest.viewmodel.HomeViewModel;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Sacra on 3/8/16.
 */
public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        ButterKnife.bind(this, view);

        this.homeViewModel = new HomeViewModel();
        this.homeViewModel.getNoticeEndPoint(this);

        return view;
    }

    @OnClick(R.id.fragment_home_btn_chargeback)
    public void onContestBuyTapped() {
        this.homeViewModel.getNoticeEndPoint(this);
    }

    public void onError(String errorMessage) {
        DialogHelper.showNeutralDialog(this.getActivity(), errorMessage);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        ButterKnife.unbind(this);
    }
}
