package com.leonardo.nubanktest.domain;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Sacra on 3/13/16.
 */
public class ReasonDetail {

    @SerializedName("id")
    private String id;

    @SerializedName("title")
    private String title;

    @SerializedName("response")
    private Boolean response;

    public ReasonDetail() {
        this.response = false;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getResponse() {
        return response;
    }

    public void setResponse(Boolean response) {
        this.response = response;
    }

    public boolean isDomainInvalid() {
        return this.id == null || this.title == null;
    }
}
