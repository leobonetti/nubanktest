package com.leonardo.nubanktest.webservice;

import com.leonardo.nubanktest.domain.ChargebackData;
import com.leonardo.nubanktest.domain.Status;
import com.leonardo.nubanktest.webservice.request.SaveContestRequest;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Url;

/**
 * Created by Sacra on 3/13/16.
 */
public interface ChargebackService {

    @GET
    Call<ChargebackData> getChargebackData(@Url String url);

    @POST
    Call<Status> blockCard(@Url String url);

    @POST
    Call<Status> unblockCard(@Url String url);

    @POST
    Call<Status> saveConstest(@Url String url, @Body SaveContestRequest saveContestRequest);
}
