package com.leonardo.nubanktest.domain;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Sacra on 3/13/16.
 */
public class Status {

    @SerializedName("status")
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isDomainInvalid() {
        return this.status == null;
    }
}
