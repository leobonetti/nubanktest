package com.leonardo.nubanktest.helpers;

import com.google.gson.Gson;

/**
 * Created by Sacra on 3/13/16.
 */
public class GsonHelper {

    public static String toJson(Object object, Class objectClass) {
        Gson gson = new Gson();
        return gson.toJson(object, objectClass);
    }

    public static <T> T fromJson(String json, Class<T> objectClass) {
        Gson gson = new Gson();
        return objectClass.cast(gson.fromJson(json, objectClass));
    }
}
