package com.leonardo.nubanktest.domain;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Sacra on 3/12/16.
 */
public class EndPoint {

    @SerializedName("href")
    private String href;

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }
}
